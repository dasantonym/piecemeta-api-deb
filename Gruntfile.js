module.exports = function (grunt) {
    'use strict';
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        debian_package: {
            main: {
                options: {
                    name: "<%= pkg.name %>",
                    maintainer: {
                        name: "Anton Koch",
                        email: "anton.koch@gmail.com"
                    },
                    short_description: "NodeJS/Restify based API server for PieceMeta.com service",
                    long_description: "NodeJS/Restify based API server for PieceMeta.com service",
                    version: "<%= pkg.version %>",
                    build_number: "0",
                    dependencies: "mongodb,apache2,memcached,nodejs,npm,build-essential",
                    postinst: {
                        src: 'lib/scripts/postinst'
                    },
                    prerm: {
                        src: 'lib/scripts/prerm'
                    }
                },
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/piecemeta-api/',
                        src: ['**/*'],
                        dest: '/opt/piecemeta-api/'
                    },
                    {
                        expand: true,
                        cwd: 'lib/apache-config/',
                        src: ['*'],
                        dest: '/etc/apache2/sites-available/'
                    },
                    {
			expand: true,
                        cwd: 'lib/scripts/',
                        src: ['piecemeta-api.conf'],
                        dest: '/etc/init/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-debian-package');

    grunt.registerTask('default', ['debian_package']);

};
